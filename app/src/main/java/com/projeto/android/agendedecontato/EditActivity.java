package com.projeto.android.agendedecontato;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class EditActivity extends AppCompatActivity {

    private Contato     contatoInfo;
    private View        layoutEdit;

    private EditText    contatoNome;
    private EditText    contatoTtelefone;
    private EditText    contaotoEmail;
    private EditText    contatoGrupo;
    private ImageButton contatoFoto;
    private Button      salvar;

    private final int CAMERA        = 1;
    private final int GALERIA       = 2;
    private final String IMAGE_DIR  = "/FotosContatos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        contatoInfo = getIntent().getParcelableExtra("contatoInfo");

        layoutEdit          = findViewById(R.id.mainLayoutEdit);
        contatoNome         = findViewById(R.id.txtNome);
        contatoTtelefone    = findViewById(R.id.txtTelefone);
        contaotoEmail       = findViewById(R.id.txtEmail);
        contatoGrupo        = findViewById(R.id.txtGrupo);
        contatoFoto         = findViewById(R.id.btnFoto);
        salvar              = findViewById(R.id.btnSalvar);



        //INICIO DA MASCARA
        SimpleMaskFormatter teldp   = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
        MaskTextWatcher teldpp      = new MaskTextWatcher(contatoTtelefone, teldp);
        contatoTtelefone.addTextChangedListener(teldpp);
        //FIM DA MASCARA

        contatoNome.setText(contatoInfo.getNome());
        contatoTtelefone.setText(contatoInfo.getTelefone());
        contaotoEmail.setText(contatoInfo.getEmail());
        contatoGrupo.setText(contatoInfo.getGrupo());

        contatoFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alerarImagem();
            }
        });

        File imgFile = new File(contatoInfo.getFoto());
        if (imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            contatoFoto.setImageBitmap(bitmap);
        }

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // String contNome = StringUtil.toMaiuscula(contatoNome.getText().toString());
                String telef = contatoTtelefone.getText().toString().replaceAll("[^0-9]", "");

                contatoInfo.setNome(contatoNome.getText().toString());
                contatoInfo.setTelefone(telef);
                contatoInfo.setEmail(contaotoEmail.getText().toString());
                contatoInfo.setGrupo(contatoGrupo.getText().toString());

                if (contatoInfo.getNome().equals("")){
                    Toast.makeText(EditActivity.this, "Nenhum conteúdo para salvar. Contato descartado",Toast.LENGTH_LONG).show();
                    return;
                }

                Intent i = new Intent();
                i.putExtra("contatoInfo", contatoInfo);
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }
    private void alerarImagem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecione a fonte a imagem");
        builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clicarTiraFoto();
            }
        });
        builder.setNegativeButton("Galeria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clicarCarragarImagem();
            }
        });
        builder.create().show();
    }
    private void clicarTiraFoto() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED){
            requestCameraPermission();
        }else{
            showCamera();
        }
    }

    private void requestCameraPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)){
            Snackbar.make(layoutEdit, "É necessário permitirpara ultilizar a câmera!!",
                    Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(EditActivity.this,
                            new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            CAMERA);
                }
            }).show();

        }else{
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CAMERA);
        }
    }

    private void  showCamera(){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, CAMERA);
    }


    private void clicarCarragarImagem() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED){
            requestGaleriaPermission();
        }else{
            showGaleria();
        }
    }

    private void requestGaleriaPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)){
            Snackbar.make(layoutEdit, "É necessário permitirpara ultilizar a galeria!!",
                    Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(EditActivity.this,
                            new String[]{
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            GALERIA);
                }
            }).show();

        }else{
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    GALERIA);
        }

    }

    private void showGaleria(){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALERIA);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    clicarTiraFoto();
                }
                break;
            case GALERIA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    clicarCarragarImagem();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESULT_CANCELED || data == null){
            return;
        }
        if (requestCode == GALERIA){
            Uri contentUri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),contentUri);
                contatoInfo.setFoto(saveImage(bitmap));
                contatoFoto.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA){
            Bitmap bitmap = (Bitmap)data.getExtras().get("data");
            contatoInfo.setFoto(saveImage(bitmap));
            contatoFoto.setImageBitmap(bitmap);
        }
    }

    private String saveImage(Bitmap bitmap){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 99, bytes);
        File directory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIR);

        if (!directory.exists()){
            directory.mkdirs();
        }


        try {
            File f = new File(directory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

            MediaScannerConnection.scanFile(this, new String[]{f.getPath()}, new String[]{"image/jpeg"}, null);
            fo.close();
            return  f.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
