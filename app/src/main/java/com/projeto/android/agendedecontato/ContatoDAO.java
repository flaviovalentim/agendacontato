package com.projeto.android.agendedecontato;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ContatoDAO extends SQLiteOpenHelper {

    private static final int VERSAO = 1;
    private final String TABELA = "Contatos";
    private static final String DATABASE = "DadosAgenda";

    public ContatoDAO(Context context) {
        super(context, DATABASE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = " CREATE TABLE " + TABELA
                + " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + " nome TEXT NOT NULL, "
                + " telefone TEXT, "
                + " email TEXT, "
                + " grupo TEXT, "
                + " foto TEXT);";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int il) {

    }

    public List<Contato> getList(String order){
        List<Contato> contatos = new ArrayList<>();

        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM " + TABELA + " ORDER BY nome " +
                order + ";", null);

        while (cursor.moveToNext()){
            Contato c = new Contato();

            c.setId(cursor.getLong(cursor.getColumnIndex("id")));
            c.setNome(cursor.getString(cursor.getColumnIndex("nome")));
            c.setTelefone(cursor.getString(cursor.getColumnIndex("telefone")));
            c.setEmail(cursor.getString(cursor.getColumnIndex("email")));
            c.setGrupo(cursor.getString(cursor.getColumnIndex("grupo")));
            c.setFoto(cursor.getString(cursor.getColumnIndex("foto")));

            contatos.add(c);
        }
        cursor.close();
        return contatos;
    }

    public void createContato(Contato c){
        ContentValues v = new ContentValues();

        v.put("nome", c.getNome());
        v.put("telefone", c.getTelefone());
        v.put("email", c.getEmail());
        v.put("grupo", c.getGrupo());
        v.put("foto", c.getFoto());

        getWritableDatabase().insert(TABELA, null, v);
    }

    public void updateContato(Contato c){
        ContentValues v = new ContentValues();

        v.put("id", c.getId());
        v.put("nome", c.getNome());
        v.put("telefone", c.getTelefone());
        v.put("email", c.getEmail());
        v.put("grupo", c.getGrupo());
        v.put("foto",c.getFoto());

        String[] idUpdate = {c.getId().toString()};
        getWritableDatabase().update(TABELA, v, "id=?", idUpdate);
    }

    public void deleteContato(Contato c){
        SQLiteDatabase db = getWritableDatabase();
        String[] itens = {c.getId().toString()};
        db.delete(TABELA, "id=?", itens);
    }
}
