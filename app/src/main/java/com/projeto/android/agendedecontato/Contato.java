package com.projeto.android.agendedecontato;

import android.os.Parcel;
import android.os.Parcelable;

public class Contato implements Parcelable {
    private Long id = -1L;
    private String nome = "";
    private String telefone = "";
    private String email = "";
    private String grupo = "";
    private String foto = "";

    public Contato(){
    }

    private Contato(Parcel in){
        String[] data = new String[6];
        in.readStringArray(data);

        setId(Long.parseLong(data[0]));
        setNome(data[1]);
        setTelefone(data[2]);
        setEmail(data[3]);
        setGrupo(data[4]);
        setFoto(data[5]);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{
                String.valueOf(getId()),
                getNome(),
                getTelefone(),
                getEmail(),
                getGrupo(),
                getFoto(),
        });
    }

    public static final Parcelable.Creator<Contato> CREATOR = new Parcelable.Creator<Contato>() {
        @Override
        public Contato createFromParcel(Parcel parcel) {
            return new Contato(parcel);
        }

        @Override
        public Contato[] newArray(int i) {
            return new Contato[i];
        }
    };
}