package com.projeto.android.agendedecontato;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.io.File;
import java.util.List;

public class ContatoAdapter extends RecyclerView.Adapter<ContatoAdapter.ContactView> {

    private List<Contato> listaContatos;

    ContatoAdapter(List<Contato> lista){
        listaContatos = lista;
    }


    @Override
    public ContactView onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_contato, viewGroup, false);
        return new ContactView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactView contactView, int i) {
        Contato c = listaContatos.get(i);
        contactView.nome.setText(c.getNome());
        contactView.telefone.setText(c.getTelefone());
        contactView.grupo.setText(c.getGrupo());

        File imgFile = new File(c.getFoto());
        if (imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            contactView.foto.setImageBitmap(bitmap);
        }
    }

    @Override
    public int getItemCount() {
        return listaContatos.size();
    }

    static class ContactView extends RecyclerView.ViewHolder {

        TextView    nome;
        TextView    telefone;
        TextView    grupo;
        ImageView   foto;


        ContactView(View itemView) {
            super(itemView);

            nome        = itemView.findViewById(R.id.textNome);
            telefone    = itemView.findViewById(R.id.textTelefone);
            grupo       = itemView.findViewById(R.id.textGrupo);
            foto        = itemView.findViewById(R.id.imageFoto);

            //INICIO DA MASCARA
            SimpleMaskFormatter teldp   = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
            MaskTextWatcher teldpp      = new MaskTextWatcher(telefone, teldp);
            telefone.addTextChangedListener(teldpp);
            //FIM DA MASCARA
        }
    }
}
