package com.projeto.android.agendedecontato;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ContatoDAO contatoDAO;

    private RecyclerView contatosListaView;
    private ContatoAdapter adapter;

    private List<Contato> listaContatos;

    private final int REQUEST_CADASTRAR = 1;
    private final int REQUEST_ALTERAR   = 2;
    private String order                = "ASC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, EditActivity.class);
                i.putExtra("contatoInfo", new Contato());
                startActivityForResult(i, REQUEST_CADASTRAR);
            }
        });

        contatoDAO      = new ContatoDAO(this);
        listaContatos   = contatoDAO.getList(order);

        contatosListaView = findViewById(R.id.contatosListaView);
        contatosListaView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        contatosListaView.setLayoutManager(llm);

        adapter = new ContatoAdapter(listaContatos);
        contatosListaView.setAdapter(adapter);

        contatosListaView.addOnItemTouchListener(new ItemClickListener(getApplicationContext(),
                new ItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        abrirOpcoes(listaContatos.get(i));
                    }
                }));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode     == REQUEST_CADASTRAR && resultCode == RESULT_OK){
            Contato contato = data.getParcelableExtra("contatoInfo");
            contatoDAO.createContato(contato);
            listaContatos   = contatoDAO.getList(order);
            adapter         = new ContatoAdapter(listaContatos);
            contatosListaView.setAdapter(adapter);
        }
        else if (requestCode    == REQUEST_ALTERAR && resultCode == RESULT_OK){
            Contato contato     = data.getParcelableExtra("contatoInfo");
            contatoDAO.updateContato(contato);
            listaContatos       = contatoDAO.getList(order);
            adapter             = new ContatoAdapter(listaContatos);
            contatosListaView.setAdapter(adapter);
        }
    }

    private void abrirOpcoes(final Contato contato){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(contato.getNome());
        builder.setItems(new CharSequence[]{"Ligar", "Editar", "Excluir"},
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        switch (i){
                            case 0:
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + contato.getTelefone()));
                                startActivity(intent);
                                break;
                            case 1:
                                Intent editar = new Intent(MainActivity.this, EditActivity.class);
                                editar.putExtra("contatoInfo", contato);
                                startActivityForResult(editar, REQUEST_ALTERAR);
                                break;

                            case 2:
                                listaContatos.remove(contato);
                                contatoDAO.deleteContato(contato);
                                adapter.notifyDataSetChanged();
                                break;

                        }
                    }
                });
        builder.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.orde_az) {
            order = "ASC";
        }else if (id == R.id.orde_za){
            order = "DESC";
        }

        listaContatos = contatoDAO.getList(order);
        adapter = new ContatoAdapter(listaContatos);
        contatosListaView.setAdapter(adapter);
        return true;
    }
}
