package com.projeto.android.agendedecontato;

class StringUtil {
    public static String toMaiuscula(String str){
        String[] palavras = str.split("\\s");
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < palavras.length; i++){
            sb.append(palavras[i].substring(0,1).toUpperCase() + palavras[i].substring(1).toLowerCase());
            sb.append(" ");
        }
        return sb.toString();
    }
}
